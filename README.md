# Api-Testing

Contains all the content used in the videos

## Content

1. Modern applications architecture (frontend----backend)
2. Data Formats 
    - Json (represent A list of two persons with all properties like name, age, address, isMarried)
    - XML
3. Client-server communication model (https://en.wikipedia.org/wiki/Client%E2%80%93server_model)
4. Communication standard (HTTP(s))
5. URI\URL
6. Media type
7. Ways to share information between client and server
8. Query parameters, request body, headers
9. Response body, http status and headers from server
10. Http status
    - GET
    - POST
    - PUT
    - PATCH
    - DELETE
11. REST
    - Resource
    - Resource identification through unique url (endpoint)
    - Representation
    - REST criteria
    - Content negotiation
12. Patterns open doors for automation (Documentation)
13. Open API Specifications
14. Swagger UI
15. Tour of sample application
16. Postman basics
    - Creating requests
    - Requests history
    - authorisation
    - Preset Headers
    - Authorization
    - Environments
    - Proxy
    - Web interface & synchronisation
    - Import
    - code generation
17. Postman Collections
    - Creation
    - Collection runners
    - Collection variables
    - Pre-request scripts
    - Data files
    - test execution
    - test data initialisation
    - Request execution from script
    - using command line
18. Postman Mock Server
    - Mock server creation
    - Faking data
    - Additional response
    - Mocking feature
    - Mocking response codes
    - Matching algorithm
19. Some practical use cases and exercises
